package com.mii.poc.microservices.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
//import org.springframework.da
import com.mii.poc.microservices.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long>{
List<Account> findById(String id);
List<Account> findAll();
}