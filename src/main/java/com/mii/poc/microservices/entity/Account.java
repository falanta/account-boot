package com.mii.poc.microservices.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "poc")
public class Account implements Serializable {
private static final long serialVersionUID = -2343243243242432341L;
@Id @GeneratedValue(generator="system-uuid")
@GenericGenerator(name="system-uuid", strategy = "uuid")
private String id;
@Column(name = "name")
private String name;
//Setters, getters and constructors

public Account() {
	
}

public Account(String id,String name) {
	this.id=id;
	this.name=name;
}

public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public static long getSerialversionuid() {
	return serialVersionUID;
}



}