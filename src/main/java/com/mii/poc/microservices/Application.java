package com.mii.poc.microservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.mii.poc.microservices.controller.AccountController;

@SpringBootApplication
@ComponentScan(basePackageClasses = AccountController.class)
public class Application {

	public static void main(String[] args) {
		System.out.println("start");
		SpringApplication.run(Application.class, args);
	}

}
