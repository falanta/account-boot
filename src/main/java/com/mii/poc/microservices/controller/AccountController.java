package com.mii.poc.microservices.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mii.poc.microservices.entity.Account;
import com.mii.poc.microservices.entity.AccountUI;
import com.mii.poc.microservices.repository.AccountRepository;

@RestController
public class AccountController {
	@Autowired
	AccountRepository repository;

	@GetMapping("/bulkcreate")
	public String bulkcreate() {
// save a single Account
		repository.save(new Account("1", "Bhojwani"));
// save a list of Accounts
		repository.saveAll(Arrays.asList(new Account("1", "Khan"), new Account("2", "Parihar"),
				new Account("4", "Dravid"), new Account("3", "Bhojwani")));
		return "Accounts are created";
	}

	@PostMapping("/create")
	public String create(@RequestBody AccountUI Account) {
// save a single Account
		repository.save(new Account(Account.getId(), Account.getName()));
		return "Account is created";
	}

	@GetMapping("/findall")
	public List<AccountUI> findAll() {
		List<Account> Accounts = repository.findAll();
		List<AccountUI> AccountUI = new ArrayList<>();
		for (Account Account : Accounts) {
			AccountUI.add(new AccountUI(Account.getId(), Account.getName()));
		}
		return AccountUI;
	}

	@RequestMapping("/search/{id}")
	public String search(@PathVariable long id) {
		String Account = "";
		Account = repository.findById(id).toString();
		return Account;
	}

	@RequestMapping("/searchbyfirstname/{name}")
	public List<AccountUI> fetchDataByFirstName(@PathVariable String id) {
		List<Account> Accounts = repository.findById(id);
		List<AccountUI> AccountUI = new ArrayList<>();
		for (Account Account : Accounts) {
			AccountUI.add(new AccountUI(Account.getId(), Account.getName()));
		}
		return AccountUI;
	}
}